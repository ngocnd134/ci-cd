# Gitlab CI/CD Fundamentals
## Chuẩn bị
-  Tài khoản gitlab
-  Virtual Private Server (AWS ,Azure)
## I. Khái niệm  

-   Continuous Integration: Là quá trình tự động build, test mã nguồn khi mã nguồn thay đổi.
-   Continuous Delivery: Là quá trình deploy mã nguồn một cách **thủ công**, Continuous Delivery thực hiện sau Continuous Integration 
-   Continuous Development: Giống với Continuous Delivery, tuy nhiên Continuous Development sẽ deploy 1 cách tự động

![CI/CD](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png)
Lợi ích:  
- Phát hiện được các bugs một cách sớm nhất  
- Tiết kiệm thời gian cho dev, không phải làm những công việc lặp đi lặp lại (build, test, deploy)  
- Đưa các tính năng mới của sản phẩm lên 1 cách nhanh nhất, nhận về các feedback 1 cách sớm nhất  

## II. CI/CD Gitlab  
1. Pipeline, Stage, Job  
   ***Pipeline*** là 1 nhóm các ***Job*** được thực thi trong 1 ***Stage***. Tất cả các job trong stage được thực thi *song song*  
   Nếu tất cả các job thành công, sẽ chuyển sang stage mới. Nếu 1 trong các job thất bại thì stage tiếp theo sẽ không được thực thi
2. Gitlab Runner (Có thể custom runner)
   - Là một ứng dụng để chạy các Job  
   - Opensource, viết bằng GO
3. gitlab-ci.yml
```yaml
image: docker:19 # Chỉ định môi trường chạy job, là các image docker (default dockerhub, có thể dùng image ở register khác)
services: # Chỉ định các service mà job cần (ex: database)
  - docker:dind 
stages:          # Khai báo các stages, nếu không khai báo stages sẽ có 3 stage mặc định là build, test, deploy
  - build
  - deploy

before_script: # Các script được chạy trước
  - docker version
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

build-job:       # Tên của job
  stage: build   # Tên stage mà job chạy
  rules: # Khai báo các rules để chỉ định xem job có được thêm vào stage hay không ? (if, changes, exists), gitlab sẽ check các rules theo thứ tự cho đến khi gặp rule đầu tiên thỏa mãn.
     - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^feature/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"'
  script: # các script chạy job
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE:latest

deploy-job:
  stage: deploy
  rules:
     - if: '$CI_COMMIT_BRANCH == "develop"'
  script:
    - mkdir -p ~/.ssh
    # Scan lấy SSH Host key cho địa chỉ IP server
    # Được kết quả bao nhiêu thì thêm vào file known_hosts
    - ssh-keyscan -H $SERVER_IP >> ~/.ssh/known_hosts
    
    # Sửa lại quyền của file known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - echo -e "$RSA_PRIVATE_KEY" > server.pem
    - cat server.pem
    - chmod 600 server.pem
    - >
      ssh -tt -i server.pem $SSH_USER@$SERVER_IP
      "sudo docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY};
      cd app/jwt;
      sudo docker-compose down;
      sudo docker pull ${CI_REGISTRY_IMAGE}:latest;
      sudo docker-compose up -d;
      sudo docker image prune -f;"
```
[Có 2 loại variable](https://docs.gitlab.com/ee/ci/variables/):  
Predefine variable: là những biến đã được gitlab định nghĩa sẵn
Custom variable: là những biến do người dùng định nghĩa.
 
[*Reference*](https://docs.gitlab.com/ee/ci/)
