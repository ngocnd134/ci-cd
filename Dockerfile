FROM maven:3.5.2-jdk-8-alpine AS MAVEN_BUILD
WORKDIR app
COPY pom.xml .
RUN mvn -B -s /usr/share/maven/ref/settings-docker.xml dependency:go-offline
COPY src ./src/
RUN mvn -e -B package --settings /usr/share/maven/ref/settings-docker.xml

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=MAVEN_BUILD ./app/target/*.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
